#!/bin/bash

SITE_HOSTNAME_LOC="HS-VPS-01"
SITE_FOLDER_LOC="docker/*"
SAVE_PATH="."

find . -type f -not -path "*.git*" -not -path "*update_site.sh*" -exec rm '{}' \;

scp -r -O "$SITE_HOSTNAME_LOC":"$SITE_FOLDER_LOC" "$SAVE_PATH"

read -p ": " AA
git add . && if [[ "$AA" == "" ]]; then git commit -m "$(date +%Y_%m_%d_%s)"; else git commit -m "$AA"; fi && git push -u origin master
