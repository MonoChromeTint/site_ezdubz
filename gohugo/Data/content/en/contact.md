+++
title = "Contact"
slug = "contact"
+++

<img src="/images/posts/contact/index.png"></img>

---

```
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256
```
Email: `729f1f632c02572c32b4c2ffe9c1b270fc3ebc14bafd07efa35deb44615c3fba`
<br>
Gravatar: *Same as Email*
<br>
Libravatar: *Same as Email*
<br>
Keyoxide: <a href="https://keyoxide.org/FA2DA6EAA6CC7AF61677C3358EFCECC37D158890">FA2DA6EAA6CC7AF61677C3358EFCECC37D158890</a>
<br>
GPG Key: <a href="https://keys.openpgp.org/search?q=FA2DA6EAA6CC7AF61677C3358EFCECC37D158890">FA2DA6EAA6CC7AF61677C3358EFCECC37D158890</a>
<br>
GIT: <a href="https://git.disroot.org/MonoChromeTint">https://git.disroot.org/MonoChromeTint</a>
<br>
KeyBase: MonoChromeTint
```
-----BEGIN PGP SIGNATURE-----

iQGzBAEBCAAdFiEE+i2m6qbMevYWd8M1jvzsw30ViJAFAmS8x4EACgkQjvzsw30V
iJBNcwwAg4EY/2af4iUYAlru5xmS/RwZ9QWZqUg8Qv95xFOR47QPhMspHJOvk0Eb
9e64smpbdcVxjf4K6O4QacFsFJB4GCkTqahJmM5E/06ANckyZshb4Ap+hw92WSBq
xbeIN4+XExnx8mQtl4fanacKeEdxBH7lxTmBFkTeZcTtup1UDvjg4xldEXSmWE5u
+2u68M//gviL8RLouW2Qk8ISjke9Eas82qaqF+4f1HL2x5AXMffg6YejX4N2+P+u
IG+3qA5GnKPol9sOFMQJW6vIgALIzJ4Nt1w4CJ/GAsCHR7V2cEdjy60jC+JqY2ul
9CklEUID0MqswocmPKoj6v7xBL8Kf7ldSUyBn3+qou9/MOSED5NJmuveo1sAmxcH
izKTKevv9pDDPyGqMhi9OHtHsL046ZZHXbHT+EN6AUpwcGinbEDNjQEIf4sXjU9I
OviPo93IeYkuNMsa1ZtS4Fw5aH2mp4jIPe3nFyY/l8Iipo0NjSKZCmHC7USOA7ms
/WfTENUR
=mQg9
-----END PGP SIGNATURE-----
```
