+++
authors = ["MonoChromeTint"]
title = ":"
date = "2023-03-23"
description = "Getting lightweight notifications from syslog-ng logs"
tags = [
    "site",
    "long-term",
    "project",
]
+++

This is Part _ of _ for this networking project!
You can find the files/scripts on our git / repo here: <URL>

---

# Prerequisites
* Syslog-ng configs should be outputting a *firehouse* of unified messages (to cover as much land as possible)
* Ntfy notification server

## Step #1: Syslog-ng
> In the previous post, it walks through how to setup the syslog server, so you can skip to the next part

If you already have an existing syslog-ng server, the only difference that needs to be added is the following:
`file("/var/log/messages-ase.json" template("$(format-json --scope all-nv-pairs)\n") perm(0640) create_dirs (yes) frac-digits(3));`

<INSERT IMG HERE>


After that is added to the 'syslog-ng.conf' file, restart and make sure it's writing logs

<INSERT IMG HERE>



---



## Step #2: Ntfy
### Setup
* Here's a basic docker-swarm example:
```
  ntfy: # Notifications # Notifications
    image: binwiederhier/ntfy
    command:
      - serve
    environment:
      - TZ=UTC    # optional: Change to your desired timezone
      - UID=1000
      - GID=1000
    volumes:
      - /home/user/docker-main/339_Ntfy/Cache:/var/cache/ntfy
      - /home/user/docker-main/339_Ntfy/Data:/etc/ntfy
    ports:
      - 30339:80
    deploy:
      replicas: 1
      placement:
        constraints: [ node.role == worker ]
```

* Here's a basic docker-compose example:
```
  ntfy: # Notifications # Notifications
    image: binwiederhier/ntfy
    container-name: ntfy
    command:
      - serve
    environment:
      - TZ=UTC    # optional: Change to your desired timezone
      - UID=1000
      - GID=1000
    volumes:
      - /home/user/docker-main/339_Ntfy/Cache:/var/cache/ntfy
      - /home/user/docker-main/339_Ntfy/Data:/etc/ntfy
    ports:
      - 30339:80
```

### Test the server:
* Command:  `curl "<Server URL>/<Topic>/publish?title=Example_Title&message=Example_Message&priority=4"`

<INSERT IMG HERE>


### Using the Python Script
There are only 4 variables that need to be edited:
* SERVERLOC - where the server is
* fileLoc - what files should be monitored?
* verbs - this is the list for key words to lookout for
* nouns - this list is for key words that prevent the notifications from triggering (aka ignore this)

Example values:
* SERVERLOC - `http://192.168.13.13:339/LogsWatcher/publish`
* fileLoc - `['/var/logs/fileOne','/var/logs/fileTwo']`
* verbs - `['Accepted publickey for', 'TriggerWord', 'logged in', 'logged out']`
* nouns - `['secretUser', 'ExcludeMe', 'hidden line']`



### Breakdown
* The files will be read entirely *once* (and looked for the keywords), then every new line will be appended/searched
* When the line matches the keywords ("verbs" list), it will make sure it doesn't contain words from the "nouns" list, if successful
* A notification will be sent to the "LogsWatcher" topic
* Boom! Now you'll know when the logs show that someone used a pubkey for login OR if "TriggerWord" pops up OR if someone "logged in" OR if someone "logged out". BUT making sure that all that "secretUser", "ExcludeMe", "hidden line" get ignore and not notified

> Note: "nouns" is needed if you have routine scripts that will match the conditions to send notifications, but can be safely ignored.

---

## Background?
YES!

### Setup
For Debian: `sudo apt-get install tmux -y`

<INSERT IMG HERE>


### Config
* Creating a session: `tmux new-session -t new_session`
* Attach to the session: `tmux attach -t new_session`
* Run the script: `python3 plogging.py`

<INSERT IMG HERE>

---

Done! Enjoy all the notifications!
