+++
authors = ["MonoChromeTint"]
title = "Coding: Where's the lamp sauce?!"
date = "2023-07-26"
description = "The source code of the site?!?!?!"
tags = [
    "site",
    "source",
    "code",
    "wut",
]
+++

git / repo: https://git.disroot.org/MonoChromeTint/site_ezdubz

---
# Small steps
> Who wants the source code of this site???? No one? Haha, here you go!


## Break down: 

### README
* A readme

### docker-compose
* The reverse proxy - just two ports
* goHugo - static sites

---

## gohugo directory
### content
* authors location
* categories stuff
* languages (en / es / ru)
* tags location

### static pages
* images
* post (html)
* security.txt

### themes
* the theme from https://github.com/luizdepra/hugo-coder/
