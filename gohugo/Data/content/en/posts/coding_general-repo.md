+++
authors = ["MonoChromeTint"]
title = "Coding: General Repo"
date = "2022-08-29"
description = ""
tags = [
    "git",
    "general",
    "repo",
    "checklist",
    "pki",
    "countries",
]
+++

Git / Repo: https://git.disroot.org/MonoChromeTint/Info_General

---

This repo has a few random things

## A PKI Script
> Due to a lack of info on the web, we created a bash script to handle _multi_-tier public key infrastructure certs
> The public version has been posted and incremental updates will be added as I come by them

## My checklist
> A simple checklist of stuff we've implemented

## Random
> Want a random list of a bunch of countries? Info in a CSV format? Bet, its there! *I don't remember where I got it from, so if you recognize it shoot me a message, so that I can give proper credit*
