+++
authors = ["MonoChromeTint"]
title = "Info: Qubes-OS"
date = "2022-08-29"
description = "My personal archive of info that has been gathered during my time using qubes-os"
tags = [
    "qubes-os",
    "archive",
    "recovery",
    "info",
]
+++

Git Repo: https://git.disroot.org/MonoChromeTint/Info_QubesOS

---

In the repo, it's split into several markdown files.

## Mullvad_DispVM
* This has basic info on using a Mullvad Disposable VM inside of Qubes

## Qubes BackUp Info
* This has info on manually recovering a backup (in the event that it's not possible with another qubes instance).

## Qubes-MSI
* Yes, I'm aware of how much of a pain it is to use nvidia with qubes. Nevertheless, I wanted to try

## README
* readme
