+++
authors = ["MonoChromeTint"]
title = "Coding: Network to Diagram (2/2)"
date = "2023-08-01"
description = "Graphing Network devices to diagrams autonomously"
tags = [
    "graphing",
    "network",
    "diagram",
    "openwrt",
    "graphviz",
]
+++

git / repo: https://git.disroot.org/MonoChromeTint/site_ezdubz_posts/src/branch/master/coding_graphing-network/ascii2Diagram

---
> NOTE: We are in no way associated with/sponsored by kroki. Their tool is just that awesome!






## Baseline
* In this part, you'll need an existing graphing solution or a running kroki instance

Naturally, if you already have an existing graphing solution you'll just be able to feed that graphviz diagram into it to get the render


## Using kroki
* For more verbose examples, go to their site: https://kroki.io/#examples
* I really really like that they made this tool! Phenomenal!

### Custom webui
Instead of parsing and sending to the API OR if you're away from proper tools. Here's a very basic webui made by us.
* It'll take the location of the kroki server (from the first text box):

<img src="/images/posts/coding_graphing-network-pii/first-box.png"></img>

* Combine it with the coding for the graph (from the second text box):

<img src="/images/posts/coding_graphing-network-pii/second-box.png"></img>

* Send it to the API server
* Paste the response from the API server into the right-side panel:

<img src="/images/posts/coding_graphing-network-pii/whole-webui.png"></img>

### Accessibility features
* Shortcut - "ALT+S" (while clicked inside the second textbox) will send off the info and paste the response image
* The "visualize!" button sends off the info to the server and updates the image
* The "Save image" button will download the current svg image
