+++
authors = ["MonoChromeTint"]
title = "Info: Personal Docker Swarm"
date = "2023-07-02"
description = "Info about my personal docker swarm stack"
tags = [
    "docker",
    "swarm",
    "ha",
    "stack",
]
+++

Code repo / Git: https://git.disroot.org/MonoChromeTint/prsnl-dockerswarm

---

## Deployment
Deployment is _automated_ with the "deploy.sh" script. Which allows quick take down and restart of stacks.

## Updating
What happens when you have a working variant of the stack? Naturally you have to update the old checkpoint. Hence "git_update.sh" is used to send off the current stack to the repo.

---
## The stack

As of 2023-07-22, the stack is broken down into the following groups:

### capybara (used as entertainment)
* bazarr - Helps with subtitles
* flaresolverr - Helps with solving captchas
* jackett - Gathers from sources
* jellyfin - Displays the movies/shows
* ombi - Frontend for requesting movies
* radarr - Goes through movies
* sonarr - Goes through tv shows
* transmission - Transmission


### home (used as utilities)
* librespeed - Internal speed testing
* nginx - Internal site
* photoprism - Photo gallery
* traggo - Keeping track of time
* whoami - Who Am I


### inters (important swarm stuff)
* agent - Swarmpit agent
* app - Swarmpit Dashboard
* nginx-ha - HA Syncing
* swarm db - Swarmpit Config Database
* swarm influxdb - Swarmpit timeseries DB


### marie (Mid-level assets)
* dokuwiki - Internal wiki
* duplicati - Backups
* gitea server - Internal git repo
* kanboard - ew Professional stuff
* lldap - Cool SSO accounting
* ntfy - Notifications
* server - goHugo
* syncthing - Geo independent syncing
