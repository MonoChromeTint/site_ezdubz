+++
authors = ["MonoChromeTint"]
title = "Coding: Network to Diagram (1/2)"
date = "2023-08-01"
description = "Graphing Network devices to diagrams autonomously"
tags = [
    "graphing",
    "network",
    "diagram",
    "openwrt",
    "graphviz",
]
+++

git / repo: https://git.disroot.org/MonoChromeTint/site_ezdubz_posts/src/branch/master/coding_graphing-network/Graphing_Script

---

## Baseline
* This is geared towards openwrt routes
* This script is geared towards more complex networks that would require hours to do by hand



## SSH into OpenWRT Router
`
ssh <Router Host name>
`

## Run the following
`arp > tmpARP; for i in $(cat tmpARP | tr -s ' ' | tr ' ' ',' | sort -V -t',' -k3 -k1 | grep -Ev '0x0'); do MATHC=$(grep "$(echo $i | cut -d',' -f1)" /tmp/dhcp.leases); echo "$(echo $MATHC | cut -d' ' -f4),$i"; done > arpList`

### Breakdown
* First, it'll get the all the connected devices: `arp > tmpARP`
* It'll cleanup the list and sort it based on VLans then IP address: `cat tmpARP | tr -s ' ' | tr ' ' ',' | sort -V -t',' -k3 -k1 | grep -Ev '0x0'`
* Next, It'll check the active leases to get the hostname: `MATHC=$(grep "$(echo $i | cut -d',' -f1)" /tmp/dhcp.leases)`
* Using the 'MATHC' variable (made in the previous step), it'll combine all the info: `echo "$(echo $MATHC | cut -d' ' -f4),$i"`
* Done! Now to save `> arpList`


<img src="/images/posts/coding_graphing-network-pi/router-arp.png"></img>



## Get back the List
`scp <Router Hostname>:arpList .`


## Executing the script
The Script relies on a few things:
* At least One Router and Switch
* Names of the lans
* Desired colors of the lans

These parts are able to be customized at the top of the script


## Processing the output
Since this only handles the network portion, the output can be added freely on an existing graph. If you're starting from scratch, encapsulate it with the following:


```
digraph G {
  bgcolor="lightblue"
  label="Entire Map"
  compound=true;

  /* BEGIN NETWORK MAPPING */
...
  /*END NETWORK MAPPING*/
}

```

---

On to part Two: https://ezdubz.org/posts/coding_graphing-network-pii/
