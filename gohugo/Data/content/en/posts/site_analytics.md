+++
authors = ["MonoChromeTint"]
title = "Site: Analytics"
date = "2023-07-19"
description = "Site Analytics"
tags = [
    "analytics",
    "python",
    "docker",
    "json",
    "nginx",
    "logging",
]
+++

The <a href="https://ezdubz.org/post/plot.html">analytics</a> of this site


More information about the process/code can be found at <a href="https://git.disroot.org/MonoChromeTint/docker-containers">https://git.disroot.org/MonoChromeTint/docker-containers</a>


---

## Basic breakdown
### Nginx
* The logs from nginx (reverse proxy) are written to a file in a json format

### Exporting
* In this case, we use scp to copy the logs to a different machine

### Docker
* Every thirty minutes, the docker container (from the git repo) builds the html from the logs.
* The html is then outputted to an easily retrievable location

### Importing
* The html is then imported to the site via a script (in the repo)

---
<iframe src="/post/plot.html" width="100%" height="%75"></iframe>


