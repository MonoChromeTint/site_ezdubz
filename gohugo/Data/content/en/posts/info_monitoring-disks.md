+++
authors = ["MonoChromeTint"]
title = "Info: Monitoring Disks periodically"
date = "2023-08-13"
description = "How to monitory disks periodically via crontab"
tags = [
    "crontab",
    "script",
    "disks",
    "monitor",
]
+++


Git / repo: https://git.disroot.org/MonoChromeTint/site_ezdubz_posts/src/branch/master/info_monitoring-disks


---

> NOTE: You'll need a gotify or ntfy server running to use this script


## Setup
* Copy the script from the repo to somewhere outside of the disk (the one that needs to be monitored)
<img src="/images/posts/info_monitoring-disks/verify-script-loc.png"></img>

* Make sure the disk is in the fstab file
<img src="/images/posts/info_monitoring-disks/fstab-verify.png"></img>


## Configuring
> Note: if the disk requires special permissions or user a different user, you'll need to add permission (for the current user) to access the disk


* Crontab: `sudo crontab -e`
* Pick #1
<img src="/images/posts/info_monitoring-disks/crontab-step-one.png"></img>

* Add the following line: `*/5 * * * * /home/user/Disk_Check.sh -d /home/user/remote/ -f health -s "http://192.168.13.13:339" -u bots -w "bots" -t "SA_Drives" -p 4`
<img src="/images/posts/info_monitoring-disks/crontab-step-two.png"></img>

* Save the file with: CTRL+S
* Exit with: CTRL+X

* Restart the cron service
<img src="/images/posts/info_monitoring-disks/crontab-step-three.png"></img>


### Breakdown
* Every five minutes it'll run the "Disk_Check.sh" script; with the following arguments:
* Inside the "/home/user/remote/" folder
* Check for the "health" file
* If there's an issue, contact the server at "http://192.168.13.13:339"
* Use the "bots" username and the "bots" password
* Use the topic "SA_Drives"
* Set the priority to '4'
