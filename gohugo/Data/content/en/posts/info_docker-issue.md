+++
authors = ["MonoChromeTint"]
title = "Info: Docker Swarm Issue"
date = "2023-08-01"
description = "Yes, Docker Swarm"
tags = [
    "docker",
    "swarm",
    "stack",
    "issue",
    "stuck"
]
+++

> This is mostly just a quick post about "fixing" an issue with docker swarm getting stuck after deploying, but not scheduling the containers

## Find Manager Nodes
`sudo docker node ls`

<img src="/images/posts/info_docker-issue/list-nodes.png"></img>


## Demote the leader
`sudo docker node demote <Node Name>`

<img src="/images/posts/info_docker-issue/demote-leader.png"></img>



### Verify the leader was demoted
`sudo docker node ls`

<img src="/images/posts/info_docker-issue/verify-demote.png"></img>



## Promote the manager node
`sudo docker node promote <Node Name>`

<img src="/images/posts/info_docker-issue/promote-node.png"></img>



### Verify the node was promoted
`sudo docker node ls`

<img src="/images/posts/info_docker-issue/verify-promote.png"></img>



## Done!
> Now just deploy the stack like normal
