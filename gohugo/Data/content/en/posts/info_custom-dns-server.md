+++
authors = ["MonoChromeTint"]
title = "Info: Custom DNS"
date = "2023-07-26"
description = "Setting up a custom DNS server with Docker"
tags = [
    "docker",
    "dns",
    "technitium",
]
+++


> NOTE: Not endorsed by Technitium. We just really like this dns solution.



## Part One: Setup
* coming soon

---

## Part Two: Long-term run

### Baseline
* The host machine has 4gb at its disposal
* The dns instance is only connected to our intranet
* The dns instance does not have *random* users
* The data shown is after the instance got reset 2023/03/19 (no, it wasn't cause of a config error *sigh*)
* 4 months worth of data (between 2023/03/19 to 2023/07/26)

### Request breakdown
<img src="/images/posts/custom-dns-server/Screenshot_01.png"></img>

* In the screenshot, the technitium server was able to handle 14 million+ queries!
* 5.4 Million were successful
* A rise past 154k requests on 2023/04/01
* A steady trend until 2023/05/13, dropping down to about 57k
* On 2023/06/11, the requests start to rise back up to 145k
* On 2023/07/07, they average at about 199k
* The peak occurs on 2023/07/10, which was 336k!
<br>
* Those 135 clients most certainly were happy

### Type/Protocol breakdown
<img src="/images/posts/custom-dns-server/Screenshot_02.png"></img>

> Types
* 8 zones (2 custom ones)
* 2.3 Million were recursive
* 9.1 million requests were cached (saving a lot of valuable bandwidth)
* 3.1 million were blocked (with the use of custom lists)
* And Only 0.32% failed!
<br>
<br>
<br>
> Protocol
* 9+ million were IPv4
* 4.6 million were IPv6
* 623k were HTTPS (Web traffic)
* 22k were PTR (domain to IP)
* 10k were TXT
* 7k were SRV (domain to IP)
* 2.1k were SVCB (service binding)
* 247 were DNSKEY (those are rooky numbers gotta pump them up)
* 250+ others

### Domains!!
<img src="/images/posts/custom-dns-server/Screenshot_03.png"></img>

* These are self explanatory, so I won't bother, but it's nice to see

### Conclusion
* The main reason we've used Technitium is because Pihole (at the time of using) didn't have an authoritative way of directing domains
* We have to stress again. This is not an Ad, we just like it 
