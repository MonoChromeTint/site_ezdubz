+++
authors = ["MonoChromeTint"]
title = "Site: Ezdubz"
date = "2023-03-23"
description = "Ezdubz"
tags = [
    "site",
    "long-term",
    "project",
]
+++

## Creation   (2023-03-23)
This domain is part of a long-term project to integrate the Ezdubz intranet with a wider audience.

## Publication   (2023-07-11)
Yes, now it is public! Hence, the ability to read this. The site will continue to publish new and backlogged projects.

---

## Phase I   (2013-2015)
This *org* has been in effect for a many number of years, however, as time progresses so does change. The idea of this site originated around 2013. We then started building its infrastructure for the next few years. Examples:
* Basic Routers (OpenWRT)
* Switches (Netgear/TP-Link)
* Network designs
* Wans
* Etc



## Phase II   (2015-2018)
After the infrastructure was more-or-less stable. Security began to be implemented in a more granular manner. Examples:
* Multi-layer VLans
* IPS/IDS
* Logging systems
* Fail-over network designs
* Proprietary *applications*
* WPA2-Enterprise
* Etc



## Phase III   (2018-2021)
Breaking into IRL. A more *physical* touch was needed for a very digital avenue. We then began adding physical projects to the portfolio. As to be expected, big-money is needed for expensive 3.7 meter antennas! We also moved towards more FOSS solutions.
* RTL-SDR (frequency knowledge increase)
* Operational Technology (PCB Designing, soldering, wire stuff apparently)



## Phase IV (2021-20*)
Coding! Yes, indeed. As projects became more involved, we seriously needed to find a methodology to incorporate intensive processes to a more *manageable* bite-size time-frame. So far:
* HTML/CSS/JS (Internal web-forms)
* Python (cross-platform compatibility programs)
* SQL/postgres/maria banners (DBs for the python/other custom programs)
* goLang (low-level CPU utilization)
* Rust (web-crawlers)
* Java (for "official" publications)
* BASH (cross machine synchronization or does this not count?)

---

## The Team

### MonoChromeTint
> thank you to MonoChromeTint for being our PR logistics main
* Management of public git repos
* Management of public vps instances
* GPG layout design

### superUser
> thank you to superUser for being part of the duo-team!
* Management of Internal git repos
* Final coding publication
* Coding backbone (one of two that operates all coding-related things)

### DRR
> thank you to DRR for being part of the duo-team!
* Handles all security related matters
* Code creation / revision / backbone
* Hosting all geo-spread irl infrastructure

### EXE MEEK
> in memory of EXE MEEK
* Big thanks to EXE MEEK for the original concept of EzDubz (and former iterations)

### HA*
> thank you to HA* for being the annoying cheerleader that keeps spirits high
