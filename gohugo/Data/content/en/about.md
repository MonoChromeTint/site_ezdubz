+++
title = "About"
description = "A simple gateway"
date = "2023-07-15"
aliases = ["about-us", "about", "contact"]
author = "Hugo Authors"
+++

This site is mainly a bridge between the `f9dd5c7f9bfffd26a394ad3fb8c6ed8c intranet` and the public facing portion
