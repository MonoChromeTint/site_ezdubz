# Repo for 'Ezdubz' Site

# Small steps
> Who wants the source code of this site???? No one? Haha, here you go!


## Break down: 

### README
* A readme

### docker-compose
* The reverse proxy - just two ports
* goHugo - static sites

---

## gohugo directory
### content
* authors location
* categories stuff
* languages (en / es / ru)
* tags location

### static pages
* images
* post (html)
* security.txt

### themes
* the theme from https://github.com/luizdepra/hugo-coder/
